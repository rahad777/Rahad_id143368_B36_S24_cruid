<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB
{

    public $id;
    public $Booktitle;
    Public $author_name;

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setdata($postvariabledata = null)
    {

        if (array_key_exists("id", $postvariabledata)) {
            $this->id = $postvariabledata['id'];
        }
        if (array_key_exists("Booktitle", $postvariabledata)) {
            $this->Booktitle = $postvariabledata['Booktitle'];
        }
        if (array_key_exists("author_name", $postvariabledata)) {
            $this->author_name = $postvariabledata['author_name'];
        }

    }

    public function store()
    {
        $arrData =array( $this->Booktitle,$this->author_name);
        $sql = "insert into book_title(book_title,author_name)VALUES
        (?,?)";
        $sth = $this->DBH->prepare($sql);
       $result=$sth->execute($arrData);

        if($result)
            Message::message("succesful data has bev succes");

        else
            Message::message("faild data has ben inserted succesfuly");
        Utility::redirect('create.php');
    }


}

